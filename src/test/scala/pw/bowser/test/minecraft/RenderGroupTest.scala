package pw.bowser.test.minecraft

import org.junit.Test
import pw.bowser.hysteria.minecraft.{RenderJob, RenderQueue, TickReceiver, TickGroup}
import scala.actors.Actor

/**
 * Date: 1/23/14
 * Time: 11:01 AM
 */
class RenderGroupTest {

  @Test def testGroupWorks() {
    var v1: Boolean = false
    var v2: Boolean = false
    var v3: Boolean = false

    val group: RenderQueue = new RenderQueue

    val v1R: RenderJob = new RenderJob {
      def render(): Unit = v1 = true
    }

    val v2R: RenderJob = new RenderJob {
      def render(): Unit = v2 = true
    }

    val v3R: RenderJob = new RenderJob {
      def render(): Unit = v3 = true
    }

    group register v1R
    group register v2R
    group register v3R

    group.render()

    assert(v1 && v2 & v3)
  }

  @Test def testJoinAtRuntimeWorks() {

    var v1: Boolean = false
    var v2: Boolean = false
    var v3: Boolean = false

    val group: RenderQueue = new RenderQueue

    val v1R: RenderJob = new RenderJob {
      def render(): Unit = v1 = true
    }

    val v2R: RenderJob = new RenderJob {
      def render(): Unit = v2 = true
    }

    val v3R: RenderJob = new RenderJob {
      def render(): Unit = v3 = true
    }


    val tickThread = new Thread(new Runnable {
      override def run(): Unit = group.render()
    })
    tickThread.start()

    group register v1R
    group register v2R
    group register v3R

    Thread.sleep(10000)

    assert(v1 && v2 && v3)
  }

  private class RenderQueueActor(private val group: RenderQueue) extends Actor {

    private var running: Boolean = true

    def act() {
      while(running){
        group.render()
        Thread.sleep(1000)
      }
    }

    def stop() = running = false
  }

}