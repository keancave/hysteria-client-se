package pw.bowser.hysteria.gui.overlay

/**
 * Block of Text that goes in the TextHUD
 *
 * Date: 2/13/14
 * Time: 9:41 AM
 */
trait TextHUDBlock {

  /**
   * Lines of text belonging to the block
   *
   * @return lines
   */
  def blockLines: Array[String]

  /**
   * Position priority (upward) in which the block would like to appear
   *
   * @return sort order
   */
  def sortOrder: Int

  /**
   * The colour of the string
   *
   * @return colour
   */
  def colour: Int

}
object TextHUDBlock {

  /**
   * Create an OTF Block of white text
   *
   * @param lines text lines
   * @return text block
   */
  def apply(lines: String*): SimpleHUDBlock = apply(0xFFFFFF, lines: _*)

  /**
   * Create an OTF block of the specified colour
   *
   * @param colour  colour
   * @param lines   text lines
   * @return        block
   */
  def apply(colour: Int, lines: String*): SimpleHUDBlock = apply(1, colour, lines: _*)

  /**
   * Create an OTF Block
   * @param sortOrder sort order: {@see TextHUDBlock#sortOrder}
   * @param lines lines of text
   * @return text block
   */
  def apply(sortOrder: Int, colour: Int, lines: String*): SimpleHUDBlock = new SimpleHUDBlock(lines.toArray, sortOrder, colour)

}