package pw.bowser.hysteria.inject

import com.google.inject.AbstractModule
import pw.bowser.libhysteria.modules.ModuleLoader

/**
 * Just a bundle to add a ModuleLoader binding because we can't with the engine bundle but would like to later on
 */
class ModLoaderBundle(loaderImpl: ModuleLoader) extends AbstractModule {

  def configure(): Unit = {
    bind(classOf[ModuleLoader]) toInstance loaderImpl
  }

}
