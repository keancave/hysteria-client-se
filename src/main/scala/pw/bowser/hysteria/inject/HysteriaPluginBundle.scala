package pw.bowser.hysteria.inject

import com.google.inject.AbstractModule
import java.io.File
import java.util.logging.{Level, SimpleFormatter, FileHandler, Logger}
import pw.bowser.libhysteria.storage.marshal.registrar.{FileStorageService, StorageService}
import pw.bowser.libhysteria.config.map.MapConfiguration
import pw.bowser.hysteria.util.saving.GlobalStorage
import pw.bowser.libhysteria.config.Configuration
import pw.bowser.libhysteria.modules.ModuleManifest

/**
 * Date: 1/29/14
 * Time: 10:15 PM
 */
class HysteriaPluginBundle(val descriptor: ModuleManifest, val storageBase: File) extends AbstractModule {

  //--------------------------------------------------------------------------------------------------------------------

  val configurationFolder: File = new File(storageBase, "settings")
  val logFolder: File           = new File(storageBase, "logs")
  val dataFolder: File          = new File(storageBase, "data")

  if(!configurationFolder.isDirectory) configurationFolder.mkdirs()
  if(!logFolder.isDirectory) logFolder.mkdirs()
  if(!dataFolder.isDirectory) dataFolder.mkdirs()

  //--------------------------------------------------------------------------------------------------------------------

  val configStoreRegistrar: StorageService  = new FileStorageService(configurationFolder, Some("conf"))
  GlobalStorage.enroll(configStoreRegistrar)

  val dataStoreRegistrar: StorageService    = new FileStorageService(dataFolder, None)
  GlobalStorage.enroll(dataStoreRegistrar)

  val pluginConfiguration: Configuration = new MapConfiguration(handle = "config")
  configStoreRegistrar.enroll(pluginConfiguration)

  //--------------------------------------------------------------------------------------------------------------------

  val logger: Logger  = Logger.getLogger(s"Plugin: ${descriptor.groupId()}.${descriptor.name()}")

  private val logFile: File = new File(logFolder, "plugin.log")

  if(!logFile.exists()) logFile.createNewFile()

  private val fileHandler: FileHandler = new FileHandler(logFile.getAbsolutePath, true)
  fileHandler.setFormatter(new SimpleFormatter)
  fileHandler.setLevel(Level.ALL)

  logger.addHandler(fileHandler)

  def configure(): Unit = {
    /*
     * Logging
     */

    bind(classOf[Logger]) annotatedWith classOf[PluginRel] toInstance logger

    /*
     * Storage
     */

    /**
     * We assume that any reference to `@Inject File` wants the plugin arbitrary data folder
     */
    bind(classOf[File]) annotatedWith classOf[PluginRel] toInstance dataFolder

    /**
     * Goodies
     */
    bind(classOf[File]) annotatedWith classOf[Config] toInstance configurationFolder

    /**
     * Looks like `@Config StorageMediaRegistrar`
     */
    bind(classOf[StorageService]) toInstance configStoreRegistrar

    bind(classOf[StorageService]) annotatedWith classOf[PluginRel] toInstance dataStoreRegistrar

    /*
     * Configuration
     */

    bind(classOf[Configuration]) annotatedWith classOf[PluginRel] toInstance pluginConfiguration

  }

}
