package pw.bowser.hysteria.display

import net.minecraft.client.gui.ScaledResolution
import org.lwjgl.input.Mouse
import net.minecraft.client.Minecraft
import pw.bowser.hysteria.engine.HysteriaBroker

/**
 * Display helper that lets us avoid notch code
 * Date: 1/17/14
 * Time: 11:32 AM
 */
object DisplayUtil {

  def applyResolution(res: ScaledResolution){
    GLUtil.configureInterfaceRendering(res.getScaledWidth_double, res.getScaledHeight_double)
  }

  def displayWidth: Int  = HysteriaBroker.hysteria.minecraft.displayWidth
  def displayHeight: Int = HysteriaBroker.hysteria.minecraft.displayHeight

  // XXX Internal scaled res access.
  // Cache bust on res change/next rq

  // TODO via wrapper
  private def guiScale: Int = HysteriaBroker.hysteria.minecraft.gameSettings.guiScale
  private var lastKnownScaleCoef = guiScale
  private var storedScaledResolution = new ScaledResolution(HysteriaBroker.hysteria.minecraft, displayWidth, displayHeight)

  private def scaledResolution: ScaledResolution = if (guiScale != lastKnownScaleCoef) {
    lastKnownScaleCoef = guiScale
    storedScaledResolution = new ScaledResolution(HysteriaBroker.hysteria.minecraft, displayWidth, displayHeight)
    storedScaledResolution
  } else {
    storedScaledResolution
  }


  def scaledDisplayWidth: Int = scaledResolution.getScaledWidth
  def scaledDisplayHeight: Int = scaledResolution.getScaledHeight

}