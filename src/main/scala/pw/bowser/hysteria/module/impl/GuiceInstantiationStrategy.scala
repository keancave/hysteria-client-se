package pw.bowser.hysteria.module.impl

import com.google.inject.{Key, Injector}
import pw.bowser.libhysteria.modules.{ModuleManifest, Module, ModuleInstantiator}
import java.io.File
import pw.bowser.hysteria.inject.{PluginRel, HysteriaPluginBundle}
import java.util.logging.Logger

/**
 * Module instantiation strategy that uses Guice
 * Date: 5/6/14
 * Time: 7:07 PM
 */
class GuiceInstantiationStrategy(rootFolder: File, parentInjector: Injector) extends ModuleInstantiator {
  override def getInstance[MT <: Module](moduleClass: Class[MT]): MT = {

    // Get manifest for plugin
    val desc = moduleClass.getAnnotation(classOf[ModuleManifest])

    val moduleStorage = new File(rootFolder, desc.name())
    val specificInjector = parentInjector.createChildInjector(new HysteriaPluginBundle(desc, moduleStorage))

    val instance = specificInjector.getInstance(moduleClass)

    instance.setLogger(specificInjector.getInstance(Key.get(classOf[Logger], classOf[PluginRel])))

    instance
  }
}
