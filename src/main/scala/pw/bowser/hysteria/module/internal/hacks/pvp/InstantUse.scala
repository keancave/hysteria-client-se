package pw.bowser.hysteria.module.internal.hacks.pvp

import net.minecraft.util.{EnumFacing, BlockPos}
import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.hysteria.commands.companion.Command
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.hysteria.minecraft.TickReceiver
import net.minecraft.init.Items
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.libhysteria.toggles.TogglesMixin
import net.minecraft.network.play.client.{C07PacketPlayerDigging, C03PacketPlayer, C08PacketPlayerBlockPlacement, C09PacketHeldItemChange}

/**
 * Date: 3/31/14
 * Time: 8:59 AM
 */
@ModuleManifest(
  groupId     = "pw.bowser",
  name        = "InstantUse",
  version     = "0.4",
  commands    = Array("iu"),
  description = "Use Bow/Items instantly"
)
class InstantUse extends HysteriaModule with TogglesMixin with TickReceiver {

  private val command = Command(Array("iu"), null.asInstanceOf[String], "InstantBow", ToggleFlag(Array("@if_none"), this))

  override def enableModule(): Unit = {
    commandDispatcher.registerCommand(command)
    toggleRegistry.enrollToggleable(this)
    super.enableModule()
  }

  override def disableModule(): Unit = {
    commandDispatcher.unRegisterCommand(command)
    toggleRegistry.disenrollToggleable(this)
    super.disableModule()
  }

  def onTick(): Unit = if(isEnabled
    && minecraftToolkit.thePlayer != null
    && minecraftToolkit.inventoryToolkit.isUsingItem
    && InstantUse.UsableItems.contains(minecraftToolkit.inventoryToolkit.currentItemStack.getItem)) {
    minecraftToolkit.sendPacket(new C08PacketPlayerBlockPlacement(new BlockPos(-1, -1, -1), -1, minecraftToolkit.inventoryToolkit.currentItemStack, 0, 0, 0))
    minecraftToolkit.sendPacket(new C09PacketHeldItemChange(minecraftToolkit.inventoryToolkit.currentHotBarSlot))
    for(i <- 0 to 30) minecraftToolkit.sendPacket(new C03PacketPlayer(false))
    minecraftToolkit.sendPacket(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.RELEASE_USE_ITEM, new BlockPos(0, 0, 0), EnumFacing.DOWN))
    minecraftToolkit.inventoryToolkit.stopUsingItem()
  }

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  def displayName: String = "InstantUse"

  /**
   * Should return the distinguished name of the toggles
   * The distinguished name should be unique to the toggles, ie, `pw.bowser.mods.mod.distinguished_name_here`
   * @return
   */
  override def distinguishedName: String = "pw.hysteria.instant_use"

  override def enable(): Unit = {
    tickService.register(this)
  }

  override def disable(): Unit = {
    tickService.remove(this)
  }
}

object InstantUse {
  val UsableItems = List( Items.apple, Items.baked_potato, Items.beef, Items.bow, Items.bread, Items.carrot, Items.chicken, Items.cooked_beef,
    Items.cooked_chicken, Items.cooked_fish, Items.cooked_porkchop, Items.cookie, Items.fish,
    Items.golden_apple, Items.golden_carrot, Items.melon, Items.milk_bucket, Items.mushroom_stew,
    Items.poisonous_potato, Items.porkchop, Items.potato, Items.potionitem, Items.pumpkin_pie, Items.rotten_flesh,
    Items.spider_eye )
}