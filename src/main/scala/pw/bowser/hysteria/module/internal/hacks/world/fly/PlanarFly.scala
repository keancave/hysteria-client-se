package pw.bowser.hysteria.module.internal.hacks.world.fly

import java.lang.Math.{pow, sin, cos, PI}
import net.minecraft.util.MathHelper.sqrt_double

/**
 * Date: 2/28/14
 * Time: 3:21 PM
 */
object PlanarFly extends FlyMode {
  /**
   * Calculate the results of the movement filter
   *
   * @param   pitch     player pitch
   * @param   yaw       player yaw
   * @param   mvStrafe  player strafe value
   * @param   mvForward player forward movement value
   * @param   motionY   player vertical movement value
   *
   * @return  (mvX, mvY, mvZ) modifiers
   */
  def computeModifiers(pitch: Double, yaw: Double, mvStrafe: Float, mvForward: Float, motionX: Double, motionY: Double, motionZ: Double, wedge: Double): (Double, Double, Double) = {

    var coMove =
      sqrt_double(pow(mvStrafe, 2) + pow(mvForward, 2))

    if (coMove < 0.01F) {
      return (0, motionY, 0)
    } else if (coMove < 1F) coMove = 1F

    coMove = 1F / coMove // 1

    val strafe = coMove * mvStrafe
    val forward = coMove * mvForward

    val modX = sin((yaw * PI).toFloat / 180F)
    val modZ = cos((yaw * PI).toFloat / 180F)

    val modYX = sin((pitch * PI).toFloat / 180F)
    val modYY = cos((pitch * PI).toFloat / 180F)

    val coForward =
      if (mvForward > 0) -1 else if (mvForward < 0) 1 else 0

    ((strafe * modZ - forward * modX) * wedge,
      motionY * wedge,
     (forward * modZ + strafe * modX) * wedge)
  }
}
