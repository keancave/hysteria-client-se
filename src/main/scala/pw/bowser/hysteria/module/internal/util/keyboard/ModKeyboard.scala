package pw.bowser.hysteria.module.internal.util.keyboard

import org.lwjgl.input.Keyboard
import pw.bowser.hysteria.chat.Formatting
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.hysteria.commands.companion.{Flag, Command}

/**
 * Manages keybinds
 * Date: 2/19/14
 * Time: 9:19 AM
 */
@ModuleManifest(
  groupId     = "pw.hysteria",
  name        = "KeyBindings",
  version     = "0.1",
  commands    = Array("key", "kb"),
  description = "Manage keyboard bindings"
)
final class ModKeyboard extends HysteriaModule {



  private val bindingsManager     = new ChatBindingsManager(keyboard, minecraftToolkit)

  private val bindingsCommand     = Command(
    Array("key", "kb"),
    Flag(
      Array("@if_none", "list"),
      {args =>
        tellPlayer(s"Bound Key Sequences: ${bindingsManager.bindings.map(_.sequence.map(Keyboard.getKeyName).mkString("+")).mkString(", ")}")
      },
      null,
      "List bound key sequences"
    ),
    Flag(
      Array("seq", "s"),
      {args =>
        val sequence = parseSequence(args(0))
        if(args.length > 1){
          bindingsManager.bind(sequence, args(1)) // TODO auto unbind bound sequences without touching bindingsManage in a bad way
        }

        bindingsManager.lookup(sequence) match {
          case binding: Some[ChatTextKeyBinding] =>
            tellPlayer(s"Sequence ${Formatting.ITALIC}${binding.get.sequence.map(Keyboard.getKeyName).mkString("+")}${Formatting.RESET} is set to ${Formatting.ITALIC}${binding.get.text}")
          case None =>
            tellPlayer(s"Sequence ${Formatting.ITALIC}${args(0)}${Formatting.RESET} is not bound to anything")
        }
      },
      "<sequence: KEY_NAME[+KEY_NAME...]> [New Value]",
      "Get(1) or Set(2) a key binding"
    ),
    Flag(
      Array("remove", "r"),
      {args =>
        bindingsManager.remove(parseSequence(args(0)))
        tellPlayer(s"Sequence ${Formatting.ITALIC}${args(0)}${Formatting.RESET} unbound!")
      }
    )
  )

  override def initModule(): Unit = {
    super.initModule()

  }

  override def enableModule(): Unit = {
    getStorageService.enroll(bindingsManager)
    commandDispatcher.registerCommand(bindingsCommand)
    super.initModule()
  }

  override def disableModule(): Unit = {
    commandDispatcher.unRegisterCommand(bindingsCommand)
    getStorageService.disenroll(bindingsManager)
    bindingsManager.disenrollAllBindings()
    super.disableModule()
  }

  def parseSequence(text: String): Array[Int] = text.split("\\+").map(Keyboard.getKeyIndex)

}

