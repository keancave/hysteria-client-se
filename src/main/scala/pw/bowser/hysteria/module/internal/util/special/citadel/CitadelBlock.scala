package pw.bowser.hysteria.module.internal.util.special.citadel

import scala.util.parsing.json.{JSONArray, JSONObject}
import pw.bowser.hysteria.module.internal.util.special.citadel.ProtectionType.ProtectionLevel

/**
 * Represents a protected block in the world
 * @param posX x-position of the protected block
 * @param posY y-position of the protected block
 * @param posZ z-position of the protected block
 * @param protectionType protection type of the block
 * @param cache citadel instance for the server that this block is on
 * @param companionBlock if the represented block is a chest, this is the other side.
 */
final class CitadelBlock(val posX:  Long, val posY:  Long, val posZ: Long,
                         var protectionType: ProtectionType, var protectionLevel: ProtectionLevel,
                         cache: CitadelProtectionCache,
                         var companionBlock: Option[(Int, Int, Int)] = None) {

  var currentResilience: Int = protectionType.resilience

  def toJson: JSONObject = JSONObject(Map(
      "protection"  -> protectionType.name,
      "level"       -> protectionLevel.levelName,
      "resilience"  -> currentResilience,
      "companion"   -> (if(companionBlock != None) JSONArray(List(companionBlock.get._1, companionBlock.get._2, companionBlock.get._3)) else JSONArray(List()))
    ))

  def fromJson(json: JSONObject): Unit = {
    protectionType    = ProtectionType.withName(json.obj("protection").asInstanceOf[String])
    protectionLevel   = ProtectionLevel.withName(json.obj("level").asInstanceOf[String])
    currentResilience = json.obj("resilience").asInstanceOf[Double].toInt

    if(currentResilience < 0) currentResilience = protectionType.resilience

    companionBlock    = json.obj("companion").asInstanceOf[JSONArray].list match {
      case List(x: Double, y: Double, z: Double) => Some((x.toInt, y.toInt, z.toInt))
      case _ => None
    }

  }

  /**
   * Get the block position
   * @return
   */
  def position: (Long, Long, Long) = (posX, posY, posZ)

  /**
   * Get the instance of the companion block
   * @return companion block instance, if any
   */
  def companionInstance: Option[CitadelBlock] = companionBlock match {
    case Some(_)  => cache.getBlockAt(companionBlock.get._1, companionBlock.get._2, companionBlock.get._2)
    case None     => None
  }
}
