package pw.bowser.hysteria.module.internal.overlay

import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.hysteria.commands.companion.Command
import pw.bowser.hysteria.config.ConfigurationFlag
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.libhysteria.toggles.TogglesMixin
import pw.bowser.hysteria.module.internal.HysteriaModule

/**
 * Date: 2/10/14
 * Time: 11:18 PM
 */
@ModuleManifest(
  groupId     = "pw.hysteria",
  name        = "Overlay",
  version     = "0.1",
  description = "Provides overlay services",
  commands    = Array("overlay", "hud")
)
class ModTextHud extends HysteriaModule with TogglesMixin {

  // Fields
  //--------------------------------------------------------------------------------------------------------------------

  // HysteriaModule
  //--------------------------------------------------------------------------------------------------------------------

  override def initModule(): Unit = {

    super.initModule()

    configuration.setPropertyIfNew("hud.colours.default", 0x00AA00)
    configuration.setPropertyIfNew("hud.opacity.default", 0xDC)

    /*
     * Hud toggle command
     */
    commandDispatcher.registerCommand(
      Command(
        Array("hud"),
        "toggle the HUD",
        ToggleFlag(Array("@if_none"), this),
        ConfigurationFlag(Array("o", "opacity"), configuration, "hud.opacity.default", ConfigurationFlag.Transforms.INT)
      )
    )

  }

  override def enableModule(): Unit = {
    toggleRegistry.enrollToggleable(this)
    headsUpDisplay.setDefaultOpacityFunction(getHudOpacity)
  }

  override def disableModule(): Unit = {
    toggleRegistry.disenrollToggleable(this)
    headsUpDisplay.setDefaultOpacityFunction({() => 0xFF})
  }

  // Helper Methods
  //--------------------------------------------------------------------------------------------------------------------

  def getHudOpacity(): Int = configuration.getInt("hud.opacity.default")

  // Toggles Mixin
  //--------------------------------------------------------------------------------------------------------------------

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  def displayName: String = "TextHUD"

  def distinguishedName: String = "pw.hysteria.text_overlay"

  override def enable() {
    renderQueue.register(headsUpDisplay)
  }

  override def disable() {
    renderQueue.remove(headsUpDisplay)
  }
}
