package pw.bowser.hysteria.module.internal.hacks.world

import pw.bowser.hysteria.minecraft.RenderJob
import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.minecraft.wrapper.net.minecraft.render.entity.HRenderManager
import scala.collection.JavaConversions._
import net.minecraft.entity.projectile.EntityArrow
import pw.bowser.hysteria.display.GLUtil
import org.lwjgl.util.glu.{GLU, Sphere}
import org.lwjgl.opengl.GL11
import pw.bowser.hysteria.commands.companion.Command
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.hysteria.config.ConfigurationFlag

import java.lang.Math.pow
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.libhysteria.toggles.TogglesMixin
import pw.bowser.hysteria.commands.HysteriaCommand

/**
 * Date: 3/7/14
 * Time: 5:50 PM
 */
@ModuleManifest(
  groupId     = "pw.bowser",
  name        = "ArrowZones",
  version     = "0.1",
  commands    = Array("az"),
  description = "Draws danger zones where arrows are expected to land"
)
class ArrowZones extends HysteriaModule with TogglesMixin with RenderJob {

  // Fields
  //-------------------------------------------------------------------------------------------------------------------

  // Module implementation
  //--------------------------------------------------------------------------------------------------------------------



  private var command: HysteriaCommand = null

  override def initModule(): Unit = {
    super.initModule()

    configuration.setPropertyIfNew("spheres.colour.launch",   0x00B658)
    configuration.setPropertyIfNew("spheres.colour.ground",   0xB63400)
    configuration.setPropertyIfNew("spheres.alpha",    0.45F)

    command = Command(Array("az"),
      ToggleFlag(Array("t", "@if_none"), this),
      ConfigurationFlag(Array("fc", "lcolour", "launchcolour"), configuration, "spheres.colour.launch", ConfigurationFlag.Transforms.INT),
      ConfigurationFlag(Array("gc", "gcolour", "groundcolour"), configuration, "spheres.colour.ground", ConfigurationFlag.Transforms.INT),
      ConfigurationFlag(Array("a", "alpha"),  configuration, "spheres.alpha",  ConfigurationFlag.Transforms.FLOAT)
    )
  }

  override def enableModule(): Unit = {
    super.enableModule()

    toggleRegistry.enrollToggleable(this)
    commandDispatcher.registerCommand(command)
  }

  override def disableModule(): Unit = {
    super.disableModule()

    toggleRegistry.disenrollToggleable(this)
    commandDispatcher.unRegisterCommand(command)
  }

  // Toggles Implementation
  //--------------------------------------------------------------------------------------------------------------------

  override def disable(): Unit = {
    worldRenderQueue.remove(this)
  }

  override def enable(): Unit = {
    worldRenderQueue.register(this)
  }

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  def displayName: String = "ArrowZones"

  def distinguishedName: String = "pw.hysteria.arrow_zones"

  // RenderJob implementation
  //--------------------------------------------------------------------------------------------------------------------

  val sphereInstance = new Sphere

  def render(): Unit = GLUtil.with3D({

    val cStart  = GLUtil.componentsOf(configuration.getInt("spheres.colour.launch"))
    val cFinish = GLUtil.componentsOf(configuration.getInt("spheres.colour.ground"))
    val cDeltas = (cFinish._1 - cStart._1, cFinish._2 - cStart._2, cFinish._3 - cStart._3)

    GL11.glLineWidth(3.6F)
    GL11.glEnable(GL11.GL_DEPTH_TEST)
    GL11.glDepthMask(true)
    minecraft.theWorld.loadedEntityList.foreach({
      case(ea: EntityArrow) =>

        val lenLive           = ea.getFlightLive.size() - 1
        val maxStrideLen      = ea.getFlightPathSimulated.length  - 1

        val posAtNow      =
          (
            ea.getFlightLive.getQuick(lenLive - 2),
            ea.getFlightLive.getQuick(lenLive - 1),
            ea.getFlightLive.getQuick(lenLive - 0)
          )

        val initialPos    =
         (
           ea.getFlightPathSimulated()(0),
           ea.getFlightPathSimulated()(1),
           ea.getFlightPathSimulated()(2)
         )

        val finalPos      =
         (
           ea.getFlightPathSimulated()(maxStrideLen - 2),
           ea.getFlightPathSimulated()(maxStrideLen - 1),
           ea.getFlightPathSimulated()(maxStrideLen - 0)
         )

        val distanceMax   = pow(finalPos._1 - initialPos._1, 2) + pow(finalPos._2 - initialPos._2, 2) + pow(finalPos._3 - initialPos._3, 2)

        val colourSlices  = (cDeltas._1 / distanceMax, cDeltas._2 / distanceMax, cDeltas._3 / distanceMax)
        val travelQuant   = pow(finalPos._1 - posAtNow._1, 2) + pow(finalPos._2 - posAtNow._2, 2) + pow(finalPos._3 - posAtNow._3, 2) / distanceMax

        GL11.glPushMatrix()

        GL11.glColor4d(cFinish._1 - (colourSlices._1 * travelQuant),
                       cFinish._2 - (colourSlices._2 * travelQuant),
                       cFinish._3 - (colourSlices._3 * travelQuant),
                       configuration.getFloat("spheres.alpha"))

        sphereInstance.setDrawStyle(if(ea.getTicksInGround > 0) GLU.GLU_FILL else GLU.GLU_LINE)

        val xOffset = finalPos._1 - HRenderManager.renderPosX
        val yOffset = finalPos._2 - HRenderManager.renderPosY
        val zOffset = finalPos._3 - HRenderManager.renderPosZ

        GL11.glTranslated(xOffset, yOffset, zOffset)

        sphereInstance.draw(1.34F, 20, 20)

        GL11.glPopMatrix()

      case _ => // Ignore
    })

  })

}
