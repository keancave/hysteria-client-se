package pw.bowser.hysteria.module.internal.hacks.world

import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.hysteria.commands.HysteriaCommand
import pw.bowser.hysteria.commands.companion.Command
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.hysteria.config.ConfigurationFlag
import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.libhysteria.toggles.{ToggleState, TogglesMixin}

/**
 * MM""""""""`M                     dP   M"""""`'"""`YM oo
 * MM  mmmmmmmM                     88   M  mm.  mm.  M
 * M'      MMMM .d8888b. .d8888b. d8888P M  MMM  MMM  M dP 88d888b. .d8888b.
 * MM  MMMMMMMM 88'  `88 Y8ooooo.   88   M  MMM  MMM  M 88 88'  `88 88ooood8
 * MM  MMMMMMMM 88.  .88       88   88   M  MMM  MMM  M 88 88    88 88.  ...
 * MM  MMMMMMMM `88888P8 `88888P'   dP   M  MMM  MMM  M dP dP    dP `88888P'
 * MMMMMMMMMMMM                          MMMMMMMMMMMMMM
 *
 * Does the fast mining
 * Date: 2/16/14
 * Time: 11:06 AM
 */
@ModuleManifest(
  groupId     = "pw.hysteria",
  name        = "FastMine",
  version     = "0.1",
  commands    = Array("fm"),
  description = "Mine things faster"
)
class FastMine extends HysteriaModule with TogglesMixin {

  var command: HysteriaCommand = null

  /**
   * Event that listens for the player damaging something below the block and says that the player is not falling
   */
//  val eventListenerBlockBelow = EventClient[PlayerDamageBlockEvent]({event =>
//    if(event.blockY < minecraft.thePlayer.posX.floor) minecraft.thePlayer.onGround = true
//  }, Int.MaxValue)

  override def initModule(): Unit = {
    super.initModule()

    configuration.setPropertyIfNew("speedmine.delay",  2)
    configuration.setPropertyIfNew("speedmine.coef",   1.3F)

    command = Command(
      Array("fm", "fastmine"),
      ToggleFlag(Array("@if_none", "t"), this),
      ConfigurationFlag[Int](Array("d", "delay"), configuration, "speedmine.delay", {(args: Array[String]) => args(0).toInt}),
      ConfigurationFlag[Float](Array("m", "multiplier"), configuration, "speedmine.coef", {(args: Array[String]) => args(0).toFloat})
    )
  }

  override def enableModule(): Unit = {
    super.enableModule()

//    eventSystem.subscribe(classOf[PlayerDamageBlockEvent], eventListenerBlockBelow)
    commandDispatcher.registerCommand(command)
    toggleRegistry.enrollToggleable(this)
  }

  override def disableModule(): Unit = {
    toggleRegistry.disenrollToggleable(this)
    commandDispatcher.unRegisterCommand(command)
//    eventSystem.unSubscribe(classOf[PlayerDamageBlockEvent], eventListenerBlockBelow)

    super.disableModule()
  }

  // Accessor methods
  //--------------------------------------------------------------------------------------------------------------------

  /**
   * Get the strength coefficient for tools
   * @return calculated strength coefficient
   */
  def strengthCoefficient: Float = getState match {
    case ToggleState.Enabled  => configuration.getFloat("speedmine.coef")
    case ToggleState.Disabled => 1
  }

  /**
   * Get the delay between hitting blocks
   * @return calculated hit delay
   */
  def hitDelay: Int  = getState match {
    case ToggleState.Enabled  => configuration.getInt("speedmine.delay")
    case ToggleState.Disabled => 5
  }

  // Toggles
  //--------------------------------------------------------------------------------------------------------------------

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  def displayName: String = "FastMine"

  /**
   * Should return the distinguished name of the toggles
   * The distinguished name should be unique to the toggles, ie, `pw.bowser.mods.mod.distinguished_name_here`
   * @return
   */
  override def distinguishedName: String = "pw.hysteria.fast_mine"
}
