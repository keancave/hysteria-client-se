package pw.bowser.hysteria.module.internal.util

import com.google.inject.Inject
import pw.hysteria.input.dashfoo.command.parsing.CommandRegexProvider
import pw.bowser.hysteria.commands.HysteriaCommand
import pw.bowser.hysteria.engine.HysteriaServices
import pw.hysteria.input.dashfoo.command.IsFlag
import scala.collection.JavaConversions._

class CommandChat @Inject() (regexProvider: CommandRegexProvider) extends HysteriaCommand(Array("c", "chat"), regexProvider) with HysteriaServices {

  def getDescription: String = "Chat tools. Settings."

  def getHelp: String = {
    var helpText = ""
    flagger.getFlags.foreach({pair =>
      helpText += s"Flag: ${pair._1} - "
      helpText += s"${pair._2.getArity} parameters; "
      if(pair._2.getHelpProvider.getDescription != null)
        helpText += pair._2.getHelpProvider.getDescription
      if(pair._2.getHelpProvider.getHelp != null)
        helpText += "; " + pair._2.getHelpProvider.getHelp
      helpText += "\n"
    })

    helpText
  }

  @IsFlag(handles = Array("h"), getDescription = "Get help", getHelp = "-h")
  def doHelp()= tellPlayer(getHelp)

  @IsFlag(
    handles = Array("say", "s"),
    getDescription = "Say something in chat",
    getHelp = "-say <text>"
  )
  def sayToServer(text: String)= sayText(text)

  @IsFlag(
    handles = Array("sethandle"),
    getDescription = "Set the command handle",
    getHelp = "e.g. -sethandle *"
  )
  def setCommandHandle(handle: String)= {
    tellPlayer(s"Set command handle to `$handle`")
    engineConfiguration.setProperty("commands.form.handle", handle)
  }

  @IsFlag(
    handles = Array("setescape"),
    getDescription = "Set the escape sequence",
    getHelp = "e.g. -setescape \\\\"
  )
  def setCommandEscape(escape: String)= {
    tellPlayer(s"Set command escape sequence to `$escape`")
    engineConfiguration.setProperty("commands.form.escape", escape)
  }

  @IsFlag(
    handles = Array("setquote"),
    getDescription = "Set the blockquote sequence",
    getHelp = "e.g. -setquote %"
  )
  def setCommandQuote(quote: String)= {
    tellPlayer(s"Set command quote sequence to `$quote`")
    engineConfiguration.setProperty("commands.form.quote", quote)
  }

  @IsFlag(
    handles         = Array("setflag"),
    getDescription  = "Set the flag sequence",
    getHelp         = "e.g. -setflag %"
  )
  def setCommandFlag(flag: String)= {
    tellPlayer(s"Set command flag sequence to `$flag`")
    engineConfiguration.setProperty("commands.form.flag", flag)
  }

}