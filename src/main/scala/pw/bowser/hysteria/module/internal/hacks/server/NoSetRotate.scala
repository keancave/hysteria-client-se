package pw.bowser.hysteria.module.internal.hacks.server

import pw.bowser.hysteria.events.EventClient
import pw.bowser.hysteria.eventsimpl.network.PacketReceiveEvent
import net.minecraft.network.play.server.S08PacketPlayerPosLook
import net.minecraft.network.play.client.C03PacketPlayer
import pw.bowser.hysteria.commands.companion.Command
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.libhysteria.toggles.TogglesMixin

/**
 * Prevents the server from setting your rotation (because NCP sucks at handling PVP)
 * Date: 4/7/14
 * Time: 11:13 AM
 */
@ModuleManifest(
  groupId     = "pw.bowser",
  name        = "NoSetRotate",
  version     = "0.1",
  description = "Prevents the setting of the player rotation by the server",
  commands    = Array("nosr")
)
class NoSetRotate extends HysteriaModule with TogglesMixin {

  private val packetRecieve = EventClient[PacketReceiveEvent]({event =>
    event.packet match {
      case position: S08PacketPlayerPosLook if isEnabled =>
        event.packet = new S08PacketPlayerPosLook(position.x, position.y, position.z,
                                                  minecraft.thePlayer.rotationYaw, minecraft.thePlayer.rotationPitch,
                                                  position.flags)
        minecraftToolkit.sendPacket(new C03PacketPlayer.C05PacketPlayerLook(minecraftToolkit.thePlayer.rotationYaw, minecraftToolkit.thePlayer.rotationPitch, minecraftToolkit.thePlayer.onGround))
      case _ =>
    }
  })

  private val command = Command(Array("nosr"), ToggleFlag(Array("@if_none"), this))

  // Module Implementation
  //--------------------------------------------------------------------------------------------------------------------

  override def enableModule(): Unit = {
    super.enableModule()

    eventSystem.subscribe(classOf[PacketReceiveEvent], packetRecieve)
    commandDispatcher.registerCommand(command)
    toggleRegistry.enrollToggleable(this)
  }

  override def disableModule(): Unit = {
    super.disableModule()

    eventSystem.unSubscribe(classOf[PacketReceiveEvent], packetRecieve)
    commandDispatcher.unRegisterCommand(command)
    toggleRegistry.disenrollToggleable(this)
  }

  // Toggles Implementation
  //--------------------------------------------------------------------------------------------------------------------

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  def displayName: String = "NoSetRotate"

  /**
   * Should return the distinguished name of the toggles
   * The distinguished name should be unique to the toggles, ie, `pw.bowser.mods.mod.distinguished_name_here`
   * @return
   */
  override def distinguishedName: String = "pw.hysteria.disable_server_rotation"
}
