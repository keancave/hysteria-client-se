package pw.bowser.hysteria.commands.companion

import pw.hysteria.input.dashfoo.command.{HelpProvider, Flag => FlagSpec}
import pw.bowser.hysteria.commands.SimpleHelpProvider

/**
 * Create flags on the fly
 *
 * Date: 2/6/14
 * Time: 10:30 AM
 */
object Flag {

  /**
   * Create a command flag OTF
   * @param handles   flag handles
   * @param action    flag action
   * @return          flag
   */
  def apply(handles: Array[String], action: Array[String] => Unit): FlagSpec = {
    apply(handles, action, null)
  }

  /**
   * Create a command flag OTF
   * @param handles   flag handles
   * @param action    flag action
   * @param help      flag help
   * @return          flag
   */
  def apply(handles: Array[String], action: Array[String] => Unit, help: String): FlagSpec = {
    apply(handles, action, help, null)
  }

  /**
   * Create a command flag OTF
   * @param handles     flag handles
   * @param action      flag action
   * @param description flag description
   * @param help        flag help
   * @return            flag
   */
  def apply(handles: Array[String], action: Array[String] => Unit, help: String, description: String): FlagSpec ={
    new InlineFlag(handles, action, new SimpleHelpProvider(help, description))
  }

  class InlineFlag(val handles: Array[String],
                   val action: Array[String] => Unit,
                   val helpProvider: HelpProvider,
                   val arity: Int = -1)
    extends FlagSpec {

    def getHandles: Array[String] = handles

    def getArity: Int = arity

    def invokeFlag(args: String*): Unit = action(args.toArray)

    def getHelpProvider: HelpProvider = helpProvider

  }

}
