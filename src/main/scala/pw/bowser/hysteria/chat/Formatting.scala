package pw.bowser.hysteria.chat

/**
 * Minecraft colours.
 *
 *
 * Date: 2/4/14
 * Time: 2:46 PM
 */
object Formatting {

  val SIGIL         = "\247"

  //--------------------------------------------------------------------------------------------------------------------

  val BLACK         = SIGIL + "0"

  val DARK_BLUE     = SIGIL + "1"

  val DARK_GREEN    = SIGIL + "2"

  val TEAL          = SIGIL + "3"

  val DARK_RED      = SIGIL + "4"

  val PURPLE        = SIGIL + "5"

  val GOLD          = SIGIL + "6"

  val GREY          = SIGIL + "7"

  val DARK_GREY     = SIGIL + "8"

  val BLUE          = SIGIL + "9"

  val BRIGHT_GREEN  = SIGIL + "a"

  val AQUA          = SIGIL + "b"

  val RED           = SIGIL + "c"

  val PINK          = SIGIL + "d"

  val YELLOW        = SIGIL + "e"

  val WHITE         = SIGIL + "f"

  //--------------------------------------------------------------------------------------------------------------------

  val BOLD          = SIGIL + "l"
  
  val UNDERLINE     = SIGIL + "n"
  
  val ITALIC        = SIGIL + "o"

  val STRIKEOUT     = SIGIL + "m"
  
  val OBFUSCATED    = SIGIL + "k"

  //--------------------------------------------------------------------------------------------------------------------

  val RESET         = SIGIL + "r"

  //--------------------------------------------------------------------------------------------------------------------
  
  def italicize(s: String): String  = Formatting.ITALIC + s + Formatting.RESET
  
  def bold(s: String): String       = Formatting.BOLD + s + Formatting.RESET
  
  def strikeout(s: String): String  = Formatting.STRIKEOUT + s + Formatting.RESET

  def underline(s: String): String  = Formatting.UNDERLINE + s + Formatting.UNDERLINE

  //--------------------------------------------------------------------------------------------------------------------

  def stripFormatting(s: String): String = s.replaceAll("(?i)\247[0-9A-FK-OR]", "")

}
