package pw.bowser.hysteria.eventsimpl.network

import pw.bowser.hysteria.events.Event
import net.minecraft.network.{Packet => MCPacket}

/**
 * Packet Event
 * Date: 3/13/14
 * Time: 10:17 PM
 */
abstract class PacketEvent(var packet: PacketEvent.Packet) extends Event {

  /**
   * Set the event packet
   * Partially because the compiler rewrites var parameters to val parameters
   * @param aPacket packet
   */
  def setPacket(aPacket: PacketEvent.Packet): Unit = packet = aPacket

}
object PacketEvent {
  type Packet = MCPacket
}