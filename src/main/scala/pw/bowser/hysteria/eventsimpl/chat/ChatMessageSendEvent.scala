package pw.bowser.hysteria.eventsimpl.chat

import pw.bowser.hysteria.events.Event
import net.minecraft.util.IChatComponent

/**
 * Event regarding the wish to transmit a chat message
 *
 * Date: 2/3/14
 * Time: 10:26 PM
 */
class ChatMessageSendEvent(var message: String) extends Event {

  def getName: String = "chat.send"

}
