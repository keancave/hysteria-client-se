package pw.bowser.boot

import pw.bowser.hysteria.engine.{Hysteria, HysteriaProvider}
import net.minecraft.client.Minecraft
import com.google.inject.Guice
import pw.bowser.hysteria.inject.HysteriaEngineBundle
import java.io.File
import pw.bowser.hysteria.inject.minecraft.MinecraftBundle

/**
 * Date: 2/21/14
 * Time: 11:15 PM
 */
class StandardHysteriaProvider extends HysteriaProvider {

  var theHysteria: Option[Hysteria] = None

  /**
   * Get hysteria
   *
   * @return Hysteria
   */
  def hysteria: Hysteria = theHysteria match {
    case h: Some[Hysteria]  => h.get
    case None               => throw UninitializedFieldError("Hysteria has not been initialized")
  }

  /**
   * Initialize hysteria and companion objects
   *
   * @param mc minecraft implementation
   */
  def initialize(mc: Minecraft): Unit = theHysteria match {
    case Some(_)  => throw new IllegalStateException("Hysteria has already been initialized")
    case None     =>

      val hysteriaStorage = new File(mc.mcDataDir, "hysteria")

      val theInjector = Guice.createInjector(new StandardVersioningBundle,
                                             new HysteriaEngineBundle(hysteriaStorage),
                                             new MinecraftBundle(mc))

      theHysteria = Some(new Hysteria(hysteriaStorage, theInjector))
      theHysteria.get.configureHysteria()
  }
}
