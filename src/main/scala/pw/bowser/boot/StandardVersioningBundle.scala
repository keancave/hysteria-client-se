package pw.bowser.boot

import com.google.inject.AbstractModule

import java.util.jar.{Manifest => JarManifest}
import pw.bowser.hysteria.engine.StaticVersionData
import pw.bowser.util.VersionData
import scala.io.Source
import java.util.Properties

/**
 * Provides runtime version data for the release client
 *
 * Date: 2/21/14
 * Time: 6:00 PM
 */
class StandardVersioningBundle extends AbstractModule {
  def configure(): Unit = {
    val buildProps  = new Properties()
    buildProps.load(getClass.getResourceAsStream("/hysteria/build.properties"))

    val sfVersion       = buildProps.getProperty("Hysteria-Version")
    val sfRevision      = buildProps.getProperty("Git-Revision")
    val sfRevisionDate  = buildProps.getProperty("Git-Revision-Date")
    val sfBuildDate     = buildProps.getProperty("Built-At")

    val versionData = new StaticVersionData(sfVersion, sfRevision, sfRevisionDate, sfBuildDate)

    bind(classOf[VersionData]) toInstance versionData
  }
}
