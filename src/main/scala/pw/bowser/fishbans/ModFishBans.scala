package pw.bowser.fishbans

import pw.bowser.hysteria.engine.HysteriaServices
import pw.bowser.hysteria.commands.companion.{Flag, Command}
import pw.bowser.hysteria.chat.Formatting
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.hysteria.module.ModuleWithServices

/**
 * Date: 2/23/14
 * Time: 12:43 AM
 */
@ModuleManifest(
  groupId     = "pw.bowser",
  name        = "FishBansData",
  version     = "0.1",
  commands    = Array("fbans"),
  description = "Creates a playerdata source that looks up from FishBans"
)
class ModFishBans extends ModuleWithServices with HysteriaServices {

  val commandFishBans = Command(
    Array("fbans"),
    Flag(
      Array("l", "lookup"),
      {args =>
        new Thread(new PlayerLookupTask(args(0), dumpProfile), s"FishBans.Lookup(${args(0)})").start()
      },
      "<Player>",
      "Show FishBans profile for a player"
    ),
    Flag(
      Array("bc", "bans"),
      {args =>
        new Thread(new PlayerLookupTask(args(0), showCount), s"FishBans.Lookup(${args(0)})").start()
      },
      "<Player>",
      "Show the number of bans a player has"
    ),
    Flag(
      Array("h", "history"),
      {args =>
        new Thread(new PlayerLookupTask(args(0), showNames), s"FishBans.Lookup(${args(0)})").start()
      },
      "<Player>",
      "Display a player's name history"
    ),
    Flag(
      Array("alts", "ac"),
      {args =>
        new Thread(new PlayerLookupTask(args(0), showNameCount), s"FishBans.Lookup(${args(0)})").start()
      },
      "<Player>",
      "Display the number of names a player has had"
    )
  )

  override def enableModule(): Unit = {
    super.enableModule()

    playerData.registerTransientDataSource(FishBansDataSource)
    commandDispatcher.registerCommand(commandFishBans)
  }

  override def disableModule(): Unit = {
    super.disableModule()

    playerData.removeTransientDataSource(FishBansDataSource)
    commandDispatcher.unRegisterCommand(commandFishBans)
  }

  // Callbacks
  //--------------------------------------------------------------------------------------------------------------------

  private def showNames(prof: Option[FishBansProfile]) = tellPlayer(prof match {
    case someProfile: Some[FishBansProfile] =>
      val profile = someProfile.get
      s"""Player ${Formatting.GREY + Formatting.italicize(profile.username)}'s Alts:
          |${profile.nameHistory.mkString(", ")}
          """.stripMargin
    case None => s"${Formatting.RED}Unable to look up player"
  })

  private def showNameCount(prof: Option[FishBansProfile]) = tellPlayer(prof match {
    case someProfile: Some[FishBansProfile] =>
      val profile = someProfile.get
      s"Player ${Formatting.GREY + Formatting.italicize(profile.username)} has ${profile.nameHistory.length} alts"
    case None => s"${Formatting.RED}Unable to look up player"
  })

  private def showCount(prof: Option[FishBansProfile]) = tellPlayer(prof match {
    case someProfile: Some[FishBansProfile] =>
      val profile = someProfile.get
      s"Player ${Formatting.GREY + Formatting.italicize(profile.username)} has ${profile.totalBans} bans"
    case None => s"${Formatting.RED}Unable to look up player"
  })

  private def dumpProfile(prof: Option[FishBansProfile]) = tellPlayer(prof match {
    case someProfile: Some[FishBansProfile] =>
      val profile = someProfile.get
      s"""Player: ${Formatting.GREY + Formatting.italicize(profile.username)} 
        |Bans: ${Formatting.underline(profile.totalBans.toString)}
        |${Formatting.GREY + Formatting.italicize("Ban Breakdown:")}
        |${Formatting.italicize("mcbans:    ") + profile(BanService.MCBANS).banCount}
        |${Formatting.italicize("mcbouncer: ") + profile(BanService.MCBOUNCER).banCount}
        |${Formatting.italicize("mcblockit: ") + profile(BanService.MCBLOCKIT).banCount}
        |${Formatting.italicize("minebans:  ") + profile(BanService.MINEBANS).banCount}
        |${Formatting.italicize("glizer:    ") + profile(BanService.GLIZER).banCount}""".stripMargin
    case None => s"${Formatting.RED}Unable to look up player"
  })

}
class PlayerLookupTask(player: String, callback: Option[FishBansProfile] => Unit) extends Runnable {
  def run(): Unit = {
    callback(FishBans.profileOf(player))
  }
}
