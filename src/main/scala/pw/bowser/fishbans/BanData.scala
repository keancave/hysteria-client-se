package pw.bowser.fishbans

/**
 * Ban Data model
 * Date: 2/23/14
 * Time: 12:59 AM
 */
class BanData(val username: String,
              val uuid:     String,
              val service:  String,
              val bans:     Map[String, String]) {

  def banCount: Int = bans.size

}
