package pw.bowser.minecraft.players

/**
 * Gets data about players and returns path mappings that are added to the player configuration
 *
 * Date: 2/14/14
 * Time: 8:18 AM
 */
trait PlayerInformationSource {

  /**
   * Get data about a player
   *
   * @param name  name of the player
   * @return      configuration data
   */
  def getDataFor(name: String): Map[String, Any]

}
