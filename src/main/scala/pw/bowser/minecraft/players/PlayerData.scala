package pw.bowser.minecraft.players

import java.io.{OutputStream, InputStream}
import scala.collection.mutable.{Map => MutableMap, HashMap => MutableHashMap}
import scala.util.parsing.json.{JSON, JSONObject}
import pw.bowser.hysteria.util.scala.json.Formatters
import scala.io.Source
import pw.bowser.hysteria.module.internal.util.friends.PlayerDataContext
import pw.bowser.libhysteria.storage.marshal.Marshalling

/**
 * Contains data about a player
 * Date: 2/13/14
 * Time: 10:07 PM
 */
class PlayerData(val realName: String) extends Marshalling {

  /**
   * Transient information is stored here
   */
  private val transientConfig = new MutableHashMap[String, Any]()

  /**
   * Persistent player information is stored in here
   */
  private val playerConfig    = new MutableHashMap[String, Any]()

  def playerInfo: MutableMap[String, Any]    = playerConfig

  def transientInfo: MutableMap[String, Any] = transientConfig

  // MVEL Integration
  //--------------------------------------------------------------------------------------------------------------------

  var mvflContext             = new PlayerDataContext(this)

  // Storage Interface
  //--------------------------------------------------------------------------------------------------------------------

  /**
   * Marshal self to output stream
   *
   * @param stream stream to write to
   */
  def writeTo(stream: OutputStream): Unit = {
    val json = JSONObject(playerInfo.toMap)

    stream.write(Formatters.prettyFormatter(json).getBytes)
  }

  /**
   * Load self from input stream
   *
   * @param stream stream to read from
   */
  def readFrom(stream: InputStream): Unit = {
    val contents  = Source.createBufferedSource(stream).getLines().mkString("\n")
    val jsonRaw   = JSON.parseRaw(contents)

    if (jsonRaw != None && jsonRaw.get.isInstanceOf[JSONObject])
      jsonRaw.get.asInstanceOf[JSONObject].obj.foreach({
        case (key, value: String) => playerConfig.put(key, value)
      })
    else
      throw new IllegalStateException(s"Read invalid data for player configuration of $realName")
  }

  /**
   * Get the handle of the marshallable (for ref like filename etc)
   *
   * @return handle
   */
  def getHandle: String = s"$realName.playerData"

  // PlayerData
  //--------------------------------------------------------------------------------------------------------------------

  /**
   * Get the appropriate display name for the player
   *
   * @return name
   */
  def nameShown = playerInfo.get("player.nickname") match {
    case nick: Some[String]   => nick.get
    case None                 => realName
  }

  /**
   * Get the friendship level of the player
   *
   * @return friendship
   */
  def friendshipOrd = playerInfo.get("player.friendship") match {
    case ord: Some[String]  =>
      ord.get match {
        case "friend"   => PlayerFriendship.FRIEND
        case "enemy"    => PlayerFriendship.ENEMY
        case "none" | _ => PlayerFriendship.NEUTRAL
      }
    case None           => PlayerFriendship.NEUTRAL
  }

  /**
   * True if the node has had persistent properties added to it
   *
   * @return modified
   */
  def isModified: Boolean = !playerInfo.isEmpty

  /**
   * Initialize data sources
   */
  def reInitializeData() {
    playerConfig.clear()
    transientConfig.clear()
    reinitializeContext()
  }

  def reinitializeContext() {
    this.mvflContext  = new PlayerDataContext(this)
  }

}
