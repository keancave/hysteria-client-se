package pw.bowser.minecraft.players

import pw.bowser.hysteria.chat.Formatting

/**
 * Describes the level of friendship a player has with the client player
 *
 * Date: 2/14/14
 * Time: 10:58 AM
 */
object PlayerFriendship {

  val FRIEND  = PlayerFriendship(Formatting.BRIGHT_GREEN, "Friend")
  val ENEMY   = PlayerFriendship(Formatting.RED, "Enemy")
  val NEUTRAL = PlayerFriendship(Formatting.WHITE, "Neutral")

}

case class PlayerFriendship(prefix: String, name: String)
