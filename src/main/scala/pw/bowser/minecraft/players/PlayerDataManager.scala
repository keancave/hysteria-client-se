package pw.bowser.minecraft.players

import scala.collection.mutable
import java.util.concurrent.{ArrayBlockingQueue, TimeUnit, ThreadPoolExecutor}
import com.google.common.util.concurrent.ThreadFactoryBuilder
import java.util.logging.{Level, Logger}
import pw.bowser.libhysteria.storage.saving.Savable
import pw.bowser.libhysteria.storage.marshal.registrar.StorageService

/**
 * Date: 2/14/14
 * Time: 2:48 PM
 */
class PlayerDataManager(val storageService: StorageService) extends Savable {


  private val executorService = new ThreadPoolExecutor(5, 8, 18, TimeUnit.SECONDS, new ArrayBlockingQueue[Runnable](200))
  executorService.setThreadFactory(new ThreadFactoryBuilder().setNameFormat("Player Lookup Thread #%d").build())

  val logger = Logger.getLogger("PlayerDataMan")
  logger.setLevel(Level.FINE)

  /**
   * Data sources are queried during data configuration for info about the player
   * The persistent data sources generate data that will be stored with the player data
   */
  private val dataSourcesPersistent = new mutable.ListBuffer[PlayerInformationSource]

  /**
   * Transient data sources are queried even at player reload for information about players that is session-specific
   */
  private val dataSourcesTransient  = new mutable.ListBuffer[PlayerInformationSource]

  /**
   * Cache of all player data requested during the session
   */
  val knownPlayerData               = new mutable.HashMap[String, PlayerData]()

  /**
   * Add a data source that will be queried for persistent data to add to each new player
   *
   * @param source data source
   */
  def registerDataSource(source: PlayerInformationSource): Unit = dataSourcesPersistent += source

  /**
   * Remove a persistent data source
   *
   * @see   PlayerDataManager#registerDataSource
   * @param source data source
   */
  def removeDataSource(source: PlayerInformationSource): Unit   = dataSourcesPersistent -= source

  /**
   * Add a data source that will be queried for transient data to add to each new player
   *
   * @param source data source
   */
  def registerTransientDataSource(source: PlayerInformationSource): Unit  = dataSourcesTransient += source

  /**
   * Remove a transient data source
   *
   * @see PlayerDataManager#registerTransientDataSource
   *@param source data source
   */
  def removeTransientDataSource(source: PlayerInformationSource): Unit    = dataSourcesTransient -= source

  /**
   * Look up a player's data. This will load from disk the player data if it does not exist in memory, or it will create a new data file.
   *
   * @param name  username of player
   * @return      player data
   */
  def lookupPlayer(name: String): PlayerData = knownPlayerData.get(name) match {
    case result: Some[PlayerData] => result.get
    case None =>
      val dataFor = new PlayerData(name)

      try{
        storageService.enroll(dataFor).load(dataFor)
      } catch {
        case is: IllegalStateException =>
        case t: Throwable => logger.log(Level.FINE, s"Not loading player from disk for $name due to an error", t)
      }

      if(!dataFor.isModified) {
        configurePersistentDataFor(dataFor)
        storageService.disenroll(dataFor)
      }

      configureTransientDataFor(dataFor)

      knownPlayerData.put(name, dataFor)
      dataFor
  }

  /**
   * Loads persistent data from sources and commits it to the PlayerData
   *
   * @param needsData data
   */
  def configurePersistentDataFor(needsData: PlayerData) =
    executorService.submit(new PlayerDataLookupThread(needsData.realName, needsData.playerInfo,  dataSourcesPersistent.toArray))

  /**
   * Loads transient data from sources and commits it to the PlayerData
   *
   * @param needsData PlayerData object
   */
  def configureTransientDataFor(needsData: PlayerData) =
    executorService.submit(new PlayerDataLookupThread(needsData.realName, needsData.transientInfo,  dataSourcesTransient.toArray))

  def save(): Unit = {
    knownPlayerData.foreach(pair => if(pair._2.isModified) storageService.enroll(pair._2))
    storageService.save()
  }

  def load(): Unit = {
    // We lazily load player data, because there is potentially a lot of it
  }
}
