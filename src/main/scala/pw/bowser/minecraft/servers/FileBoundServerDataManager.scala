package pw.bowser.minecraft.servers

import scala.collection.{mutable => m}
import java.io.File
import pw.bowser.hysteria.util.saving.GlobalStorage

import net.minecraft.client.multiplayer.{ServerData => MCServerData}
import pw.bowser.libhysteria.storage.marshal.registrar.FileStorageService

/**
 * Manages server data caching and instances.
 * Handles runtime instantiation, etc....
 * Date: 3/24/14
 * Time: 2:56 PM
 */
class FileBoundServerDataManager(dataParent: File) extends ServerDataManager {

  /**
   * Cache of server data
   */
  private val runtimePool         = m.Map[String, ServerData]()

  /**
   * Tasks that will be run with a new ServerData instance is created
   * These tasks serve as a hook for 3rd-party modifications to attach companions to server data
   */
  private val dataInitializeTasks = m.ListBuffer[ServerData => _]()

  /**
   * Get the ServerData for the specified server
   * @param mcData  minecraft server data
   * @return        server data
   */
  def getDataFor(mcData: MCServerData): ServerData = runtimePool.get(mcData.serverIP) match {
    case s: Some[ServerData] => s.get
    case None =>
      val serverFolder = new File(dataParent, mcData.serverIP.replaceAll(":", "_"))
      if(!serverFolder.exists()) serverFolder.mkdirs()

      /*
       * There are some use cases where abstract storage is not appropriate (loggers, continuous write)
       */
      val arbitraryStorage  = new File(serverFolder, "rawStorage")
      if(!arbitraryStorage.exists()) arbitraryStorage.mkdirs()

      val serverStorage     = new FileStorageService(serverFolder, None)
      val serverData        = new ServerData(mcData, arbitraryStorage, serverStorage)
      runtimePool.put(mcData.serverIP, serverData)
      GlobalStorage.enroll(serverData)

      dataInitializeTasks.foreach(_(serverData))
      serverData.load()

      serverData
  }

  /**
   * Flush the ServerData cache
   */
  def flushPool(): Unit = {
    runtimePool.foreach({case(key, data) =>
      data.save()
      GlobalStorage.disenroll(data)
    })
    runtimePool.clear()
  }

  /**
   * Add an initialization hook for ServerData creation
   * @param hook initialization hook
   */
  def registerInitializeHook(hook: ServerData => _): Unit = if(!dataInitializeTasks.contains(hook)) dataInitializeTasks += hook

  /**
   * Remove an initialization hook
   * @param hook initialization hook
   */
  def removeInitializeHook(hook: ServerData => _): Unit = dataInitializeTasks -= hook
}
object FileBoundServerDataManager {

  val SERVER_KEY_FORMAT = "%s:%d"

}
