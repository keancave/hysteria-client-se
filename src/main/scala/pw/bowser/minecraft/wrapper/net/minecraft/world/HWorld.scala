package pw.bowser.minecraft.wrapper.net.minecraft.world

import net.minecraft.block.Block
import net.minecraft.block.state.IBlockState
import net.minecraft.util.{BlockPos, MovingObjectPosition}
import net.minecraft.world.World
import pw.bowser.minecraft.wrapper.MCTypes.HVec3

/**
 * Date: 2/14/16
 * Time: 2:58 PM
 */
class HWorld(val mcWorld: World) extends AnyVal {

    import pw.bowser.minecraft.wrapper.net.minecraft.util.HVector3Support.hv3asmcv3

    def rayTraceBlocks(v1: HVec3, v2: HVec3): MovingObjectPosition = mcWorld.rayTraceBlocks(v1, v2)

    def rayTraceBlocks(v1: HVec3, v2: HVec3, stopInLiquid: Boolean): MovingObjectPosition = mcWorld.rayTraceBlocks(v1, v2, stopInLiquid)

    def getBlockState(x: Int, y: Int, z: Int): IBlockState = mcWorld.getBlockState(new BlockPos(x, y, z))

    def getBlockAt(x: Int, y: Int, z: Int): Block = getBlockState(x, y, z).getBlock
}
