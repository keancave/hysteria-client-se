package pw.bowser.minecraft.wrapper.net.minecraft.entity.player

import net.minecraft.client.entity.EntityPlayerSP

/**
 * Date: 2/13/16
 * Time: 10:12 PM
 */
final class HEntityPlayerSP(val mcEntityPlayerSP: EntityPlayerSP) extends AnyVal {

    def sendChatMessage(message: String) = mcEntityPlayerSP.sendChatMessage(message)

}
