package pw.bowser.minecraft.wrapper.net.minecraft.block

import net.minecraft.block.Block

/**
 * Date: 2/13/16
 * Time: 11:50 PM
 */
class HBlock {

}

object HBlock {
    def withId(id: Int): Option[Block] = Option(Block.getBlockById(id))

    def withName(name: String): Option[Block] = Option(Block.getBlockFromName(name))

    def withNameOrId(nameOrId: String): Option[Block] = withName(nameOrId) match {
        case it: Some[Block] => it
        case None => try withId(nameOrId.toInt) catch {
            case _: NumberFormatException => None
        }
    }
}
