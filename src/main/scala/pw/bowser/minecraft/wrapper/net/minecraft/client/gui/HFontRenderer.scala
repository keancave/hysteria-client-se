package pw.bowser.minecraft.wrapper.net.minecraft.client.gui

import net.minecraft.client.Minecraft
import net.minecraft.client.gui.FontRenderer

/**
 * Date: 2/13/16
 * Time: 9:19 PM
 */
final class HFontRenderer(val mcFontRenderer: FontRenderer) extends AnyVal {

    import pw.bowser.minecraft.wrapper.Wrapper

    def mcDrawString = {
        Wrapper.invokeMethod[FontRenderer, Int](
            "func_175065_a",
            classOf[String], classOf[Float], classOf[Float], classOf[Int], classOf[Boolean]
        ) _
    }

    def stringWidth(str: String): Int = mcFontRenderer.getStringWidth(str)

    def drawString(str: String, posX: Float, posY: Float, colour: Int, dropShadow: Boolean = false) = {
        mcDrawString(mcFontRenderer, Seq(str, posX, posY, colour, dropShadow))
    }

    def drawStringWithShadow(str: String, posX: Float, posY: Float, colour: Int) = {
        drawString(str, posX, posY, colour, dropShadow = true)
    }

}

object HFontRenderer {

    val FontHeight = 9

}
