package pw.bowser.util.interop

/**
 * Date: 3/15/14
 * Time: 10:35 PM
 */
object OptionUtil {

  def orNull[X](opt: Option[X]): X = opt match {
    case real: Some[X] => real.get
    case None => null.asInstanceOf[X]
  }

}
