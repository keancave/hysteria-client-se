package pw.bowser.util

import scalaj.http.{HttpOptions, Http}
import scala.util.parsing.json.{JSONType, JSON}

/**
 * Gets the JSON data at the url
 * Date: 2/23/14
 * Time: 12:51 AM
 */
object JSONAt {

  def apply(url: String, params: (String, String)*): Option[JSONType] = {
    JSONAt(url, Map[String, String](), 15000, params: _*)
  }

  def apply(url: String, timeout: Int, params: (String, String)*): Option[JSONType] = JSONAt(url, Map[String, String](), timeout, params: _*)

  def apply(url: String, headers: Map[String, String], params: (String, String)*): Option[JSONType] = apply(url, headers, 15000, params: _*)

  def apply(url: String, headers: Map[String, String], timeout: Int, params: (String, String)*): Option[JSONType] = {
    val response = Http(url)
      .option(HttpOptions.connTimeout(timeout))
      .option(HttpOptions.readTimeout(timeout))
      .headers(headers)
      .params(params: _*)
      .asString

    JSON.parseRaw(response)
  }

}
