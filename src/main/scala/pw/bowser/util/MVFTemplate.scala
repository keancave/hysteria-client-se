package pw.bowser.util

import scala.collection.mutable.{Map => MutableMap}
import scala.collection.JavaConversions._
import java.io.{Serializable => JSerializable}
import org.mvel2.templates.{TemplateRegistry, TemplateRuntime}

/**
 * MVEL Transformer with compiled expression caching
 * Date: 2/23/14
 * Time: 7:15 PM
 */
object MVFTemplate {

  def apply(toLex: String, tokens: Map[String, AnyRef]): String = TemplateRuntime.eval(toLex, mapAsJavaMap(tokens)).toString

  def apply(toLex: String, context: MVFLex.Context): String = TemplateRuntime.eval(toLex, context.executionContext, mapAsJavaMap(context.contextVars)).toString

  def apply(toLex: String, context: MVFLex.Context, templates: TemplateRegistry): String = TemplateRuntime.eval(toLex, context.executionContext, mapAsJavaMap(context.contextVars), templates).toString

}

