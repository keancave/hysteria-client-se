package pw.bowser.hysteria.inject;

import com.google.inject.BindingAnnotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines a dependency that is used for engine-wide things
 *
 * Date: 1/29/14
 * Time: 10:17 PM
 */
@BindingAnnotation @Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER, ElementType.LOCAL_VARIABLE})
public @interface Engine {
}
